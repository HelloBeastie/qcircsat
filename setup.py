from setuptools import setup

setup(name='qcircsat', 
        version='0.1', 
        description='Modules for comparing quantum and classical algorithms for CIRCUIT-SAT', 
        url='', 
        author='Peter Demetriou', 
        author_email='peter.demetriou@students.wits.ac.za', 
        license='Apache-2.0',
        packages=['qcircsat'],
        zip_safe=False
)
