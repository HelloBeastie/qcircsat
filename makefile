.PHONY = help setup test run-classical run-vqe run-qaoa clean-run

.DEFAULT_GOAL = setup

help:
	@echo "To install qcircsat type make setup"
	@echo "To run the classical optimiser type make run-classical"
	@echo "To run the vqe optimiser type make run-vqe"
	@echo "To run the qaoa optimiser type make run-qaoa"
	@echo "To clean the build artefacts type make clean-build"
	@echo "To clean the run artefacts type make clean-run"

setup:
	sudo python setup.py install

run-classical:
	python masters/numpy_solver.py

run-vqe:
	python masters/vqe.py

run-qaoa:
	python masters/qaoa.py

clean-build:
	sudo rm -r build/ dist/ *.egg-info/
	
clean-run:
	rm *txt *png *json *wcnf
