import networkx as nx
import random
import numpy as np
import matplotlib.pyplot as plt
from network2tikz import plot
import tikzplotlib
import argparse
import math
import os
import re
import fnmatch
import json
import datetime
from qiskit import IBMQ
from qiskit.compiler import transpile, assemble
from qiskit.transpiler import PassManager, CouplingMap, Layout
from qiskit.transpiler.passes import Unroller, StochasticSwap
from qiskit.visualization import plot_error_map
import qiskit.test.mock as Fake
from qiskit.providers.aer.noise import NoiseModel
from qiskit.circuit import QuantumCircuit, QuantumRegister, Parameter
from qiskit.providers.aer import AerSimulator
from qiskit.algorithms import QAOA
from qiskit.circuit.library import RXGate
from qiskit.algorithms.optimizers import SPSA, COBYLA
from qiskit.utils import QuantumInstance, algorithm_globals
from pysat.formula import WCNF
from circsat.preprocess import Tseitin
from circsat.postprocess import Graph
from qcircsat.problems import Problems
from qcircsat.qtools import Postprocess, Qtools, Error

def write_result(filename, result):
    with open(f'{filename}.json', 'w') as _:
        json.dump(result, _, indent=4)

def find_match(directory, string):
    files = list()
    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, string):
            files.append(file)
    return files

parser = argparse.ArgumentParser('qaoa.py')
parser.add_argument(
        'circsat_folder', 
        help='Defines the folder name of the CIRCSAT problem to be run. ' \
            + 'Usually takes values: onetwodecoder, halfadder, halfsubtractor, ' \
            + 'twoonemux, fulladder, twofourdecoder, fullsubtractor', 
        type=str
)
parser.add_argument(
        'ansatz_type', 
        help='Defines type of ansatz to be used. ' \
            + 'Can take values: Hamiltonian, Custom',  
        type=str
)
parser.add_argument(
        'maxiter', 
        help='Defines how many iterations are used. ' \
            + 'Values are usually 50 to 100.',  
        type=int
)
args = parser.parse_args()

wd = f'circuits/{args.circsat_folder}/'
exact_wd = f'{wd}eigensolver/'
sd = f'{wd}qaoa/'
td = f'{sd}tex/'
if not os.path.exists(td): os.makedirs(td)

problem_files = sorted(find_match(wd, 'E*.wcnf'))
circuit_files = sorted(find_match(wd, 'C*.json'))

for cf, pf in zip(circuit_files, problem_files):
    circuit = json.load(open(f'{wd}{cf}', 'r'))
    wcnf = WCNF(from_file=f'{wd}{pf}')
    problem = re.sub(r'\.wcnf', r'', pf)

    T = Tseitin(wcnf)
    E = T.edges
    G = T.graph
    on, off, overlap = T.graph_on_off

    phase, mixer = Problems.qaoa_max_independent_set(G, constrained=True, on=on, off=off)

    MAX_SHOTS = 1024
    MAX_CIRCUITS = 900
    SEED = 42
    random.seed(SEED)
    algorithm_globals.random_seed = SEED
    nodes = list(G.nodes)
    random.shuffle(nodes)

    ansatz_type = args.ansatz_type
    rotation_test = [
            v for v in nodes if v 
            not in on and v not in off
    ]
    if not rotation_test:
        ansatz_type = 'Hamiltonian'
    backend_type = 'CPU'
    optimiser_type = 'COBYLA'
    init_pt = None
    init_pt = np.array([[0.5, 0.5], [0.5, 0.5]]).flatten()
    p = int(len(init_pt)/2)
    n = phase.num_qubits

    if ansatz_type == 'Hamiltonian':
        s = QuantumCircuit(n)
    elif ansatz_type == 'Custom':
        s = QuantumCircuit(n)
        for v in nodes:
            if v in on:
                s.x(v)
        s = s.reverse_bits()
        pz = QuantumCircuit(n)
        gamma = Parameter(r'-\gamma')
        for v in nodes:
            pz.rz(gamma, v)
        pz = pz.reverse_bits()
        mixer = QuantumCircuit(n)
        beta = Parameter(r'2\beta')
        for v in nodes:
            ctrl=list(G.neighbors(v))
            trgt=[v]
            mocrx=RXGate(beta).control(
                num_ctrl_qubits=len(ctrl),
                ctrl_state=0
            )
            if v not in on and v not in off:
                mixer.append(mocrx, qargs=ctrl + trgt)
        mixer = mixer.reverse_bits()

    if optimiser_type == 'COBYLA':
        opt = COBYLA(maxiter=args.maxiter, tol=0.1)
    elif optimiser_type == 'SPSA':
        opt = SPSA(maxiter=args.maxiter)

    if backend_type == 'CPU':
        backend = AerSimulator(method='automatic')
        backend.set_options(precision='double', device='CPU', 
                max_parallel_threads=0, max_parallel_experiments=0)
        instance = QuantumInstance(backend, shots=MAX_SHOTS, optimization_level=3, 
                skip_qobj_validation=True, seed_simulator=SEED, seed_transpiler=SEED)
        instance = QuantumInstance(
            shots=MAX_SHOTS, 
            skip_qobj_validation=True,
            seed_simulator=SEED, 
            seed_transpiler=SEED,
            backend=backend
        )
    elif backend_type == 'NOISY_CPU':
        IBMQ.load_account()
        provider = IBMQ.get_provider(hub='ibm-q-wits', 
                group='internal', project='wits-eie-circuit')
        backend = provider.get_backend('ibmq_qasm_simulator')
        fake_backend = Fake.FakeMumbai()
        noise_model = NoiseModel.from_backend(fake_backend)
        coupling = fake_backend.configuration().coupling_map
        basis = fake_backend.configuration().basis_gates
        instance = QuantumInstance(
            shots=MAX_SHOTS, 
            skip_qobj_validation=True,
            seed_simulator=SEED, 
            seed_transpiler=SEED,
            backend=backend,
            basis_gates=basis,
            coupling_map=coupling,
            noise_model=noise_model
        )

    gset = set(G.nodes) - set(on) - set(off)
    sol_hist = list()
    i = 0

    print(f'Problem has {len(circuit)} gates and {len(G.nodes())} vertices')

    while True:
        i += 1
        qt = Qtools()
        qaoa = QAOA(optimizer=opt, reps=p, initial_state=s, mixer=mixer, 
                initial_point=init_pt, include_custom=True, callback=qt.cb, 
                quantum_instance=instance, max_evals_grouped=MAX_CIRCUITS)
        result = qaoa.compute_minimum_eigenvalue(operator=phase)

        post = Postprocess(result, G)

        approx_bs = post.most_likely
        most_sampled = Postprocess.coloured_nodes(approx_bs)
        print(f'\nFor problem {problem}:\n')
        print(f'\tTime: {post.run_time:.2f}')
        print(f'\tEnergy: {post.energy:.2f}')
        print(f'\tMost likely solution: {most_sampled}')
        
        if len(sol_hist) != 0:
            if (len(sol_hist[-1]) > len(most_sampled) or 
            sol_hist[-1] == most_sampled):
                print(f'Final solution: {sol_hist}')
                break
        sol_hist.append(most_sampled)

        graph_to_gates = Graph([most_sampled], wcnf.soft, circuit, T.vertex_enc)
        mcsc = graph_to_gates.clauses
        mcsg = graph_to_gates.minimal_corrected_gates()
        print(f'\tMinimal corrective subset clause: {mcsc}')
        print(f'\tMinimal corrective gate subset: {mcsg}')
        post.write_graph(f'{sd}RUN{i}_GRAPH_{problem}')
        inverse_transform = {
            'min_corr_sub_clauses': mcsc,
            'min_corr_sub_gates': mcsg
        }

        file = find_match(exact_wd, f'RUN{i}_RESULT_{problem}.json')
        if not file: break
        exact_result = json.load(open(f'{exact_wd}{file[0]}', 'r'))
        exact_bs = exact_result['eigenstate']
        ee = Error.expectation_sampler(phase, exact_bs)
        print(f'\tExact Expectation for {exact_bs}: {ee}')
        ae = Error.expectation_sampler(phase, approx_bs)
        print(f'\tApproximate Expectation for {approx_bs}: {ae}')

        mis_objective = Error.mis_obj(exact_bs)
        approx_mis_objective = Error.mis_obj(approx_bs)
        max_f = Error.max_factor(mis_objective, approx_mis_objective)
        ind_f = Error.ind_factor(G, most_sampled)
        mis_f = Error.mis_factor(ind_f, max_f)

        error_results = {
                '<exact>': ee,
                '<approx>': ae,
                'exact_obj_cost': mis_objective,
                'approx_obj_cost': approx_mis_objective,
                'm_factor': max_f,
                'is_factor': ind_f,
                'mis_factor': mis_f
        }
        print(f'Error: \n{error_results}\n')
        write_result(f'{sd}RUN{i}_ERROR_{problem}', error_results)
        
        t_ansatz = transpile(qaoa.ansatz, basis_gates=['x', 'sx', 'rz', 'cx'], 
                optimization_level=3)
        optimiser_results = {
                'ansatz': qaoa.ansatz.__class__.__name__,
                'optimiser': opt.__class__.__name__,
                'backend': backend.__class__.__name__,
                '#qubits': n,
                '#parameters': len(result.optimal_point),
                '#cnots': t_ansatz.count_ops().get('cx'),
                'width': t_ansatz.width(),
                'depth': t_ansatz.depth(),
                'optimiser_evaluations': qt.counts,
                'energy': qt.means,
                'parameters': [param for param in qt.params]
        }
        write_result(f'{sd}RUN{i}_OPTIMISER_{problem}', optimiser_results)
        problem_definition = {
                'circuit': circuit,
                'n_gates': len(circuit),
                'problem_name': f'Run {i} for {problem}'
        }
        post.write_result(f'{sd}RUN{i}_RESULT_{problem}', 
                problem_definition,
                inverse_transform,
                error_results,
                optimiser_results)

        plt.plot(qt.counts, qt.means)
        plt.ylabel('Energy')
        plt.xlabel('Optimiser Evaluations')
        plt.grid(color='gray', linestyle='--')
        plt.savefig(f'{sd}RUN{i}_OPTIMISER_{problem}', bbox_inches='tight')
        tikzplotlib.save(f'{td}RUN{i}_OPTIMISER_{problem}.tex')
        plt.close()

        plt.barh([*post.likely_states], np.abs([*post.likely_states.values()])**2)
        plt.ylabel('Sampled Bitstrings')
        plt.xlabel('Probability')
        plt.grid(color='gray', linestyle='--')
        plt.savefig(f'{sd}RUN{i}_HISTOGRAM_{problem}', bbox_inches='tight')
        tikzplotlib.save(f'{td}RUN{i}_HISTOGRAM_{problem}.tex')
        plt.close()

        post.draw_graphs()
        plt.savefig(f'{sd}RUN{i}_GRAPH_{problem}', bbox_inches='tight')
        plt.close()
        
        print(f'gates = {qaoa.ansatz.count_ops()}')
        print(f'depth = {qaoa.ansatz.depth()}')
        if ansatz_type == 'Custom':
            qaoa.ansatz.draw(output='mpl', justify='left', plot_barriers=False, 
                    filename=f'{sd}RUN{i}_ANSATZ_{problem}', style='iqx')
            plt.close()

            qaoa.ansatz.draw(output='latex_source', fold=-1, justify='left', plot_barriers=False, 
                    filename=f'{td}RUN{i}_ANSATZ_{problem}.tex')

        style = dict()
        style['node_size'] = 0.5
        style['node_label'] = [_ for _ in G.nodes()]
        style['node_color'] = ['gray' if node in most_sampled 
                else 'white' for node in G.nodes()]
        #style['node_style'] = {'LineWidth=0.5pt'}
        #style['edge_style'] = {LineWidth=0.5pt}
        style['node_opacity'] = 0.5
        style['edge_curved'] = 0
        plot(G, f'{td}RUN{i}_GRAPH_{problem}.tex', layout=nx.drawing.layout.circular_layout(G), **style)

        if backend.__class__.__name__ == 'IBMQBackend':
            break

        gset -= set(most_sampled)
        if not gset:
            print(f'Final solution: {sol_hist}')
            break
        bias = random.choice(tuple(gset))
        print(f'Biasing node: {bias}')
        if ansatz_type == 'Hamiltonian':
            phase, mixer = Problems.qaoa_max_independent_set(G, constrained=True, 
                    on=on, off=off, bias=bias)
        elif ansatz_type == 'Custom':
            phase, unused = Problems.qaoa_max_independent_set(G, constrained=True, 
                    on=on, off=off, bias=bias)
            s = QuantumCircuit(n)
            for v in nodes:
                if v in on:
                    s.x(v)
            s.x(bias)
            s = s.reverse_bits()

    graph_to_gates = Graph(sol_hist, wcnf.soft, circuit, T.vertex_enc)
    print(f'\nMinimal corrective subset clauses: {graph_to_gates.clauses}')
    print(f'Minimal corrective gate subset: {graph_to_gates.minimal_corrected_gates()}\n')
