import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import random
import datetime
import time
import argparse
import fnmatch
import json
import os
import re
from network2tikz import plot
from qiskit.algorithms import NumPyMinimumEigensolver
from qiskit.utils import QuantumInstance, algorithm_globals
from pysat.formula import WCNF
from qcircsat.problems import Problems
from qcircsat.qtools import Postprocess, Error
from circsat.preprocess import Tseitin
from circsat.postprocess import Graph

def find_match(directory, string):
    files = list()
    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, string):
            files.append(file)
    return files

def write_result(filename, result):
    with open(f'{filename}.json', 'w') as _:
        json.dump(result, _, indent=4)

parser = argparse.ArgumentParser('numpy_solver.py')
parser.add_argument(
        'circsat_folder', 
        help='Defines the folder name of the CIRCSAT problem to be run. ' \
            + 'Usually takes values: onetwodecoder, halfadder, halfsubtractor, ' \
            + 'twoonemux, fulladder, twofourdecoder, fullsubtractor', 
        type=str
)
args = parser.parse_args()

wd = f'circuits/{args.circsat_folder}/'
sd = f'{wd}eigensolver/'
td = f'{sd}tex/'
if not os.path.exists(td): os.makedirs(td)

problem_files = sorted(find_match(wd, 'E*.wcnf'))
circuit_files = sorted(find_match(wd, 'C*.json'))

SEED = 42
random.seed(SEED)
algorithm_globals.random_seed = SEED

for cf, pf in zip(circuit_files, problem_files):
    circuit = json.load(open(f'{wd}{cf}', 'r'))
    wcnf = WCNF(from_file=f'{wd}{pf}')
    problem = re.sub(r'\.wcnf', r'', pf)

    T = Tseitin(wcnf)
    E = T.edges
    G = T.graph
    on, off, overlap = T.graph_on_off

    cost = Problems.vqe_max_independent_set(G, on=on, off=off)
    gset = set(G.nodes) - set(on) - set(off)
    sol_hist = list()
    i = 0

    print(f'Problem has {len(circuit)} gates and {len(G.nodes())} vertices')

    while True:
        i += 1
        es = NumPyMinimumEigensolver()
        start = time.perf_counter()
        exact_result = es.compute_minimum_eigenvalue(cost)
        end = time.perf_counter()
        exact_time = round(end - start, 4)
        print(f'\nExecution time for NumPyMinimumEigensolver is {exact_time} s')
        post = Postprocess(exact_result, G) 
        
        print(f'Exact energy: {exact_result.eigenvalue.real}')
        bitstring = post.most_likely
        exact_most_sampled = Postprocess.coloured_nodes(bitstring)
        print(f'Exact Most likely solution: {bitstring}')
        print(f'Exact Most likely solution: {exact_most_sampled}')

        if len(sol_hist) != 0:
            if (len(sol_hist[-1]) > len(exact_most_sampled) or 
            sol_hist[-1] == exact_most_sampled):
                print(f'Exact final solution: {sol_hist}')
                break
        sol_hist.append(exact_most_sampled)

        graph_to_gates = Graph([exact_most_sampled], wcnf.soft, circuit, T.vertex_enc)
        result = {
                'eigenvalue': exact_result.eigenvalue.real,
                'optimiser_time': exact_time,
                'eigenstate': bitstring,
                'min_corr_sub_cl': graph_to_gates.clauses,
                'min_corr_sub_gates': graph_to_gates.minimal_corrected_gates(),
                'circuit': circuit,
                'n_gates': len(circuit),
                'num_vertices': len(G.nodes()),
                'problem_name': f'Run {i} for {problem}'
        }
        write_result(f'{sd}/RUN{i}_RESULT_{problem}', result)
        post.write_graph(f'{sd}/RUN{i}_GRAPH_{problem}')

        ee = Error.expectation_sampler(cost, bitstring)
        print(f'Exact Expectation: {ee}')
        mis_objective = Error.mis_obj(bitstring)
        print(f'MIS Objective Function Exact: {mis_objective}')

        post.draw_graph()
        plt.savefig(f'{sd}/RUN{i}_GRAPH_{problem}', bbox_inches='tight')
        plt.close()

        style = dict()
        style['node_size'] = 0.5
        style['node_label'] = [_ for _ in G.nodes()]
        style['node_color'] = ['gray' if node in exact_most_sampled 
                else 'white' for node in G.nodes()]
        #style['node_style'] = {'LineWidth=0.5pt'}
        #style['edge_style'] = {LineWidth=0.5pt}
        style['node_opacity'] = 0.5
        style['edge_curved'] = 0
        plot(G, f'{td}RUN{i}_GRAPH_{problem}.tex', layout=nx.drawing.layout.circular_layout(G), **style)
        
        gset -= set(exact_most_sampled)
        if not gset:
            print(f'Exact final solution: {sol_hist}')
            break
        bias = random.choice(tuple(gset))
        print(f'Biasing node: {bias}')
        cost = Problems.vqe_max_independent_set(G, on=on, off=off, bias=bias)
        
    bit_hist = Postprocess.bitstrings(G, sol_hist)
    print(f'\nExact final bitstring solution: {bit_hist}')
    graph_to_gates = Graph(sol_hist, wcnf.soft, circuit, T.vertex_enc)
    print(f'Minimal corrective subset clauses: {graph_to_gates.clauses}')
    print(f'Minimal corrective gate subset: {graph_to_gates.minimal_corrected_gates()} for {problem} in {cf}\n')
