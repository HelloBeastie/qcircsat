import networkx as nx
import random
import numpy as np
import matplotlib.pyplot as plt
import more_itertools as mit
from network2tikz import plot
import tikzplotlib
import argparse
import math
import os
import re
import fnmatch
import json
import datetime
from qiskit import IBMQ
from qiskit.compiler import transpile, assemble
from qiskit.transpiler import PassManager, CouplingMap, Layout
from qiskit.transpiler.passes import Unroller, StochasticSwap
from qiskit.visualization import plot_error_map
from qiskit.circuit import QuantumCircuit, QuantumRegister, ParameterVector
from qiskit.circuit.library import TwoLocal
from qiskit.algorithms.optimizers import SPSA, COBYLA
from pysat.formula import WCNF
from circsat.preprocess import Tseitin
from circsat.postprocess import Graph
from qcircsat.problems import Problems
from qcircsat.qtools import Postprocess, Qtools, Error

j_ids = list()
def jcb(*args):
    job_id, (ev, p, en, s) = args
    j_ids.append(job_id)
    cb['evaluations'].append(ev)
    cb['parameters'].append(p)
    cb['energy'].append(en)
    cb['stddev'].append(s)

def write_result(filename, result):
    with open(f'{filename}.json', 'w') as _:
        json.dump(result, _, indent=4)

def find_match(directory, string):
    files = list()
    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, string):
            files.append(file)
    return files

def qpu_execution_time(result):
    history = result['optimizer_history']
    timestamps = history['time']
    optimiser_time = result['optimizer_time']
    # ibm notes the median and mean might differ since iteration times fluctuate 
    # becuase of hourly device calibration
    runtimes = np.concatenate(([0], np.diff(timestamps)))
    total_execution = np.sum(runtimes)
    print(f'Total time taken on QPU: {total_execution:.2f}s out of optimiser time: {optimiser_time:.2f}s')
    print(f'Median time of the iterations: {np.median(runtimes):.2f}s')
    print(f'Average time per iteration: {np.mean(runtimes):.2f}s')
    print(f'Standard deviation: {np.std(runtimes):.2f}s')
    return total_execution

parser = argparse.ArgumentParser('rtvqe.py')
parser.add_argument(
        'circsat_folder',
        help='Defines the folder name of the CIRCSAT problem to be run. ' \
            + 'Usually takes values: onetwodecoder, halfadder, halfsubtractor, ' \
            + 'twoonemux, fulladder, twofourdecoder, fullsubtractor',
        type=str
)
parser.add_argument(
        'sol_start',
        help='Defines what index of problem is the first problem solved. ' \
            + 'Can take whole values usually usually starts at 0 and increments ' \
            + 'as solutions are saved',
        type=int
)
parser.add_argument(
        'sol_stop',
        help='Defines what index minus one of problem is the last problem solved. ' \
            + 'Can take natural values usually usually starts at 1 and increments ' \
            + 'as solutions are saved',
        type=int
)
parser.add_argument(
        'ansatz_type',
        help='Defines type of ansatz to be used. ' \
            + 'Can take values: TwoLocal, Custom',
        type=str
)
parser.add_argument(
        'maxiter',
        help='Defines how many iterations are used for QN-SPSA. ' \
            + 'Values are usually 100 to 200.',
        type=int
)
args = parser.parse_args()

wd = f'circuits/{args.circsat_folder}/'
exact_wd = f'{wd}eigensolver/'
sd = f'{wd}rtvqe/'
td = f'{sd}tex/'
if not os.path.exists(td): os.makedirs(td)

sol_start = args.sol_start
sol_stop = args.sol_stop
problem_files = sorted(find_match(wd, 'E*.wcnf'))[sol_start:sol_stop]
circuit_files = sorted(find_match(wd, 'C*.json'))[sol_start:sol_stop]

# 8192 is MAX
MAX_SHOTS = 1024
SEED = 42
qpu = 'ibmq_manhattan'
backend_options = {'backend_name': qpu}

for cf, pf in zip(circuit_files, problem_files):
    circuit = json.load(open(f'{wd}{cf}', 'r'))
    wcnf = WCNF(from_file=f'{wd}{pf}')
    problem = re.sub(r'\.wcnf', r'', pf)

    T = Tseitin(wcnf)
    E = T.edges
    G = T.graph
    on, off, overlap = T.graph_on_off

    cost = Problems.vqe_max_independent_set(G, on=on, off=off)

    n = cost.num_qubits
    ansatz_type = args.ansatz_type
    rotation_test = [
            q for q in range(n) if q 
            not in on and q not in off
    ]
    if not rotation_test:
        ansatz_type = 'TwoLocal'

    if ansatz_type == 'TwoLocal':
        ansatz = TwoLocal(
                n,
                ['ry'],
                'cx',
                reps=1,
                skip_unentangled_qubits=True,
                skip_final_rotation_layer=False,
                entanglement='linear',
                parameter_prefix=r'\theta'
        )
    elif ansatz_type == 'Custom':
        params = ParameterVector(r'\theta', length=n*10)
        it = iter(params)
        qubits = range(n)
        new_qubits = [
                q for q in qubits if q
                not in on and q not in off
        ]
        ansatz = QuantumCircuit(n)
        for q in qubits:
            if q in on: ansatz.x(q)
        for group in mit.consecutive_groups(new_qubits):
            qg = list(group)
            if len(qg) < 2:
                ansatz.rx(next(it), qg[0])
            else:
                for q in qg[:-1]: 
                    ansatz.cx(q, q + 1)
                ansatz.cx(qg[-1], qg[0])
        for q in new_qubits: 
            ansatz.ry(next(it), q)
        ansatz = ansatz.reverse_bits()

    np.random.seed(SEED)
    init_pt = np.random.random(ansatz.num_parameters)

    IBMQ.load_account()
    provider = IBMQ.get_provider(hub='ibm-q-wits', 
            group='internal', project='wits-eie-circuit')
    backend = provider.get_backend(qpu)
    coupling = backend.configuration().coupling_map
    coupling_map = CouplingMap(couplinglist=coupling)
    basis = backend.configuration().basis_gates
    ansatz = transpile(ansatz, basis_gates=basis, optimization_level=3, 
					   layout_method='noise_adaptive', routing_method='stochastic')

    gset = set(G.nodes) - set(on) - set(off)
    sol_hist = list()
    i = 0

    cb = {
        'evaluations': [],
        'parameters': [],
        'energy': [],
        'stddev': []
    }

    print(f'Problem has {len(circuit)} gates and {len(G.nodes())} vertices')

    while True:
        i += 1
        vqe_inputs = {
            'ansatz': ansatz,
            'operator': cost,
            'optimizer': {'name': 'QN-SPSA', 'maxiter': args.maxiter},
            'initial_point': init_pt,
            'measurement_error_mitigation': False,
            'shots': MAX_SHOTS
        }

        job = provider.runtime.run(
            program_id='vqe',
            inputs=vqe_inputs,
            options=backend_options,
            callback=jcb
        )
        print(f'JID: {job.job_id()}')
        result = job.result()

        post = Postprocess(result, G)
        approx_bs = post.most_likely
        most_sampled = Postprocess.coloured_nodes(approx_bs)
        print(f'\nFor problem {problem}:\n')
        print(f'\tTime: {post.run_time:.2f}')
        print(f'\tEnergy: {post.energy:.2f}')
        print(f'\tMost likely solution: {most_sampled}')

        if len(sol_hist) != 0:
            if (len(sol_hist[-1]) > len(most_sampled) or 
            sol_hist[-1] == most_sampled):
                print(f'Final solution: {sol_hist}')
                break
        sol_hist.append(most_sampled)

        graph_to_gates = Graph([most_sampled], wcnf.soft, circuit, T.vertex_enc)
        mcsc = graph_to_gates.clauses
        mcsg = graph_to_gates.minimal_corrected_gates()
        print(f'\tMinimal corrective subset clause: {mcsc}')
        print(f'\tMinimal corrective gate subset: {mcsg}')
        post.write_graph(f'{sd}RUN{i}_GRAPH_{problem}')
        inverse_transform = {
            'min_corr_sub_clauses': mcsc,
            'min_corr_sub_gates': mcsg
        }

        if os.path.exists(exact_wd):
            file = find_match(exact_wd, f'RUN{i}_RESULT_{problem}.json')
            if not file: break
            exact_result = json.load(open(f'{exact_wd}{file[0]}', 'r'))
            exact_bs = exact_result['eigenstate']
            ee = Error.expectation_sampler(cost, exact_bs)
            exact_gtg = Graph([Postprocess.coloured_nodes(exact_bs)], wcnf.soft, circuit, T.vertex_enc)
            print(f'\tExact Expectation for {exact_bs}({exact_gtg.clauses}, {exact_gtg.minimal_corrected_gates()}): {ee}')
            ae = Error.expectation_sampler(cost, approx_bs)
            print(f'\tApproximate Expectation for {approx_bs}: {ae}\n')

            mis_objective = Error.mis_obj(exact_bs)
            approx_mis_objective = Error.mis_obj(approx_bs)
            max_f = Error.max_factor(mis_objective, approx_mis_objective)
            ind_f = Error.ind_factor(G, most_sampled)
            mis_f = Error.mis_factor(ind_f, max_f)

            error_results = {
                    '<exact>': ee,
                    '<approx>': ae,
                    'exact_obj_cost': mis_objective,
                    'approx_obj_cost': approx_mis_objective,
                    'm_factor': max_f,
                    'is_factor': ind_f,
                    'mis_factor': mis_f
            }
            print(f'Error: \n{error_results}\n')
            write_result(f'{sd}RUN{i}_ERROR_{problem}', error_results)
        else:
            error_results = dict()
            
        loss = result['optimizer_history']['loss']
        optimiser_results = {
                'ansatz': ansatz.__class__.__name__,
                'optimiser': 'QN-SPSA',
                'backend': backend.__class__.__name__,
                'qpu': qpu,
                '#qubits': n,
                '#parameters': len(result['optimal_point']),
                '#cnots': ansatz.count_ops().get('cx'),
                'width': ansatz.width(),
                'depth': ansatz.depth(),
                'optimiser_evaluations': cb['evaluations'], # this is cost function evals (or measurements)
                'energy': cb['energy'],
                'optimiser_iterations': loss, # this is the average energy of cost function evaluations
                'qpu_time': qpu_execution_time(result),
                'parameters': [param.tolist() for param in cb['parameters']]
        }       
        write_result(f'{sd}RUN{i}_OPTIMISER_{problem}', optimiser_results)

        problem_definition = {
                'circuit': circuit,
                'n_gates': len(circuit),
                'problem_name': f'Run {i} for {problem}'
        }
        
        post.write_result(f'{sd}RUN{i}_RESULT_{problem}', 
                problem_definition,
                inverse_transform,
                error_results,
                optimiser_results)

        plt.plot([_ for _ in range(len(loss))], loss)
        plt.ylabel('Energy')
        plt.xlabel('Optimiser Iterations')
        plt.grid(color='gray', linestyle='--')
        plt.savefig(f'{sd}RUN{i}_OPTIMISER_{problem}', bbox_inches='tight')
        tikzplotlib.save(f'{td}RUN{i}_OPTIMISER_{problem}.tex')
        plt.close()

        plt.barh([*post.likely_states], np.abs([*post.likely_states.values()])**2)
        plt.ylabel('Sampled Bitstrings')
        plt.xlabel('Probability')
        plt.grid(color='gray', linestyle='--')
        plt.savefig(f'{sd}RUN{i}_HISTOGRAM_{problem}', bbox_inches='tight')
        tikzplotlib.save(f'{td}RUN{i}_HISTOGRAM_{problem}.tex')
        plt.close()

        post.draw_graphs()
        plt.savefig(f'{sd}RUN{i}_GRAPH_{problem}', bbox_inches='tight')
        plt.close()

        ansatz.draw(output='mpl', justify='left', plot_barriers=False, 
                filename=f'{sd}RUN{i}_ANSATZ_{problem}', style='iqx')
        plt.close()

        ansatz.draw(output='latex_source', fold=-1, justify='left', plot_barriers=False, 
                filename=f'{td}RUN{i}_ANSATZ_{problem}.tex')

        style = dict()
        style['node_size'] = 0.5
        style['node_label'] = [_ for _ in G.nodes()]
        style['node_color'] = ['gray' if node in most_sampled
                else 'white' for node in G.nodes()]
        style['node_opacity'] = 0.5
        style['edge_curved'] = 0
        plot(G, f'{td}RUN{i}_GRAPH_{problem}.tex', layout=nx.drawing.layout.circular_layout(G), **style)

        fig = plot_error_map(backend)
        fig.savefig(f'{sd}RUN{i}_MAP_{problem}', bbox_inches='tight')
        fig.clf()
        break # break stops it from finding every MIS which can be costly
        # on current ibm system, one at a time for now

        gset -= set(most_sampled)
        if not gset:
            print(f'Final solution: {sol_hist}')
            break
        bias = random.choice(tuple(gset))
        print(f'Biasing node: {bias}')
        if ansatz_type == 'Custom': 
            params = ParameterVector(r'\theta', length=n*10)
            it = iter(params)
            ansatz = QuantumCircuit(n)
            for q in qubits:
                if q in on: ansatz.x(q)
            ansatz.x(bias)
            for group in mit.consecutive_groups(new_qubits):
                qg = list(group)
                if len(qg) < 2:
                    ansatz.rx(next(it), qg[0])
                else:
                    for q in qg[:-1]: 
                        ansatz.cx(q, q + 1)
                    ansatz.cx(qg[-1], qg[0])
            for q in new_qubits: 
                ansatz.ry(next(it), q)
            ansatz = ansatz.reverse_bits()
        cost = Problems.vqe_max_independent_set(G, on=on, off=off, bias=bias)

    graph_to_gates = Graph(sol_hist, wcnf.soft, circuit, T.vertex_enc)
    print(f'\nMinimal corrective subset clauses: {graph_to_gates.clauses}')
    print(f'Minimal corrective gate subset: {graph_to_gates.minimal_corrected_gates()}\n')
