import networkx as nx
import random
import numpy as np
import matplotlib.pyplot as plt
import more_itertools as mit
from network2tikz import plot
import tikzplotlib
import argparse
import math
import os
import re
import fnmatch
import json
import datetime
from qiskit import IBMQ
from qiskit.compiler import transpile, assemble
from qiskit.transpiler import PassManager, CouplingMap, Layout
from qiskit.transpiler.passes import Unroller, StochasticSwap
from qiskit.visualization import plot_error_map
import qiskit.test.mock as Fake
from qiskit.providers.aer.noise import NoiseModel
from qiskit.circuit import QuantumCircuit, QuantumRegister, ParameterVector
from qiskit.providers.aer import AerSimulator
from qiskit.algorithms import VQE
from qiskit.circuit.library import TwoLocal
from qiskit.algorithms.optimizers import SPSA, COBYLA
from qiskit.utils import QuantumInstance, algorithm_globals
from pysat.formula import WCNF
from circsat.preprocess import Tseitin
from circsat.postprocess import Graph
from qcircsat.problems import Problems
from qcircsat.qtools import Postprocess, Qtools, Error

j_ids = list()
def jcb(job_id, job_status, queue_position, job):
    j_ids.append(job_id)

def write_result(filename, result):
    with open(f'{filename}.json', 'w') as _:
        json.dump(result, _, indent=4)

def find_match(directory, string):
    files = list()
    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, string):
            files.append(file)
    return files

def job_summary(ids):
    total_execution = 0
    total_runtime = 0
    ids = list(dict.fromkeys(ids))
    print('\nMost recently completed job first.')
    for j_id in reversed(ids):
        job = backend.retrieve_job(j_id)
        time_steps = job.time_per_step()
        execution = (time_steps["COMPLETED"] -
                    time_steps["RUNNING"]).total_seconds()
        runtime = (time_steps["COMPLETED"] -
                    time_steps["CREATING"]).total_seconds()
        total_runtime += runtime
        total_execution += execution
        print(f'Quantum circuit executed {j_id} in {execution}')
    c = job.circuits()[0]
    c.remove_final_measurements()
    c.draw(output='mpl', justify='left', plot_barriers=False, 
            filename=f'{sd}RUN{i}_TANSATZ_{problem}', style='iqx')
    plt.close()
    c.draw(output='latex_source', fold=-1, justify='left', 
            plot_barriers=False, filename=f'{sd}RUN{i}_TANSATZ_{problem}.tex')
    print(f'Quantum circuit execution time is {total_execution} out of runtime: {total_runtime}')
    return c, total_execution

parser = argparse.ArgumentParser('vqe.py')
parser.add_argument(
        'circsat_folder', 
        help='Defines the folder name of the CIRCSAT problem to be run. ' \
            + 'Usually takes values: onetwodecoder, halfadder, halfsubtractor, ' \
            + 'twoonemux, fulladder, twofourdecoder, fullsubtractor', 
        type=str
)
parser.add_argument(
        'ansatz_type', 
        help='Defines type of ansatz to be used. ' \
            + 'Can take values: TwoLocal, Custom',  
        type=str
)
parser.add_argument(
        'backend_type', 
        help='Defines whether a cpu, noisy cpu or qpu is used to run algorithm. ' \
            + 'Can take values: CPU, NOISY_CPU, QPU',  
        type=str
)
parser.add_argument(
        'optimiser_type', 
        help='Defines what optimiser is used. ' \
            + 'Can take values: COBYLA, SPSA',  
        type=str
)
parser.add_argument(
        'maxiter', 
        help='Defines how many iterations are used. ' \
            + 'Values are usually 100 to 200.',  
        type=int
)
args = parser.parse_args()

wd = f'circuits/{args.circsat_folder}/'
exact_wd = f'{wd}eigensolver/'
sd = f'{wd}vqe/'
td = f'{sd}tex/'
if not os.path.exists(td): os.makedirs(td)

problem_files = sorted(find_match(wd, 'E*.wcnf'))
circuit_files = sorted(find_match(wd, 'C*.json'))

# 8192 is MAX
MAX_SHOTS = 1024
MAX_CIRCUITS = 900
SEED = 42
random.seed(SEED)
algorithm_globals.random_seed = SEED

for cf, pf in zip(circuit_files, problem_files):
    circuit = json.load(open(f'{wd}{cf}', 'r'))
    wcnf = WCNF(from_file=f'{wd}{pf}')
    problem = re.sub(r'\.wcnf', r'', pf)

    T = Tseitin(wcnf)
    E = T.edges
    G = T.graph
    on, off, overlap = T.graph_on_off

    cost = Problems.vqe_max_independent_set(G, on=on, off=off)

    n = cost.num_qubits
    ansatz_type = args.ansatz_type
    rotation_test = [
            q for q in range(n) if q 
            not in on and q not in off
    ]
    if not rotation_test:
        ansatz_type = 'TwoLocal'
    backend_type = args.backend_type
    qpu = 'ibmq_brooklyn' if backend_type == 'QPU' else None
    optimiser_type = args.optimiser_type
    init_pt = None

    if ansatz_type == 'TwoLocal':
        ansatz = TwoLocal(
                n, 
                ['ry'], 
                'cx', 
                reps=1, 
                skip_unentangled_qubits=True, 
                skip_final_rotation_layer=False, 
                entanglement='linear', 
                parameter_prefix=r'\theta'
        )
    elif ansatz_type == 'Custom':
        params = ParameterVector(r'\theta', length=n*10)
        it = iter(params)
        qubits = range(n)
        new_qubits = [
                q for q in qubits if q 
                not in on and q not in off
        ]
        ansatz = QuantumCircuit(n)
        for q in qubits:
            if q in on: ansatz.x(q)
        for group in mit.consecutive_groups(new_qubits):
            qg = list(group)
            if len(qg) < 2:
                ansatz.rx(next(it), qg[0])
            else:
                for q in qg[:-1]: 
                    ansatz.cx(q, q + 1)
                ansatz.cx(qg[-1], qg[0])
        for q in new_qubits: 
            ansatz.ry(next(it), q)
        ansatz = ansatz.reverse_bits()

    if optimiser_type == 'COBYLA':
        opt = COBYLA(maxiter=args.maxiter, tol=0.1) #200 if CPU
    elif optimiser_type == 'SPSA':
        opt = SPSA(maxiter=args.maxiter)

    if backend_type == 'CPU':
        backend = AerSimulator(method='automatic')
        backend.set_options(precision='double', device='CPU', 
                max_parallel_threads=0, max_parallel_experiments=0)
        instance = QuantumInstance(backend, shots=MAX_SHOTS, 
                seed_simulator=SEED, seed_transpiler=SEED)
    elif backend_type == 'NOISY_CPU':
        IBMQ.load_account()
        provider = IBMQ.get_provider(hub='ibm-q-wits', 
                group='internal', project='wits-eie-circuit')
        backend = provider.get_backend('ibmq_qasm_simulator')
        fake_backend = Fake.FakeBrooklyn()
        noise_model = NoiseModel.from_backend(fake_backend)
        coupling = fake_backend.configuration().coupling_map
        basis = fake_backend.configuration().basis_gates
        instance = QuantumInstance(
            shots=MAX_SHOTS, 
            skip_qobj_validation=True,
            seed_simulator=SEED, 
            seed_transpiler=SEED,
            backend=backend,
            basis_gates=basis,
            coupling_map=coupling,
            noise_model=noise_model
        )
    elif backend_type == 'QPU':
        IBMQ.load_account()
        provider = IBMQ.get_provider(hub='ibm-q-wits', 
                group='internal', project='wits-eie-circuit')
        backend = provider.get_backend(qpu)
        coupling = backend.configuration().coupling_map
        coupling_map = CouplingMap(couplinglist=coupling)
        basis = backend.configuration().basis_gates
        u = Unroller(basis)
        ss = StochasticSwap(coupling_map=coupling_map)
        pm = PassManager([ss, u])
        ansatz = transpile(ansatz, basis_gates=basis)
        instance = QuantumInstance(backend, shots=MAX_SHOTS, optimization_level=3, 
                skip_qobj_validation=True, seed_simulator=SEED, seed_transpiler=SEED, 
                job_callback=jcb, pass_manager=pm)

    gset = set(G.nodes) - set(on) - set(off)
    sol_hist = list()
    i = 0

    print(f'Problem has {len(circuit)} gates and {len(G.nodes())} vertices')

    while True:
        i += 1
        qt = Qtools()
        vqe = VQE(ansatz, opt, init_pt, callback=qt.cb, quantum_instance=instance,
                include_custom=True, max_evals_grouped=MAX_CIRCUITS)
        result = vqe.compute_minimum_eigenvalue(operator=cost)

        post = Postprocess(result, G)
        approx_bs = post.most_likely
        most_sampled = Postprocess.coloured_nodes(approx_bs)
        print(f'\nFor problem {problem}:\n')
        print(f'\tTime: {post.run_time:.2f}')
        print(f'\tEnergy: {post.energy:.2f}')
        print(f'\tMost likely solution: {most_sampled}')

        if len(sol_hist) != 0:
            if (len(sol_hist[-1]) > len(most_sampled) or 
            sol_hist[-1] == most_sampled):
                print(f'Final solution: {sol_hist}')
                break
        sol_hist.append(most_sampled)

        graph_to_gates = Graph([most_sampled], wcnf.soft, circuit, T.vertex_enc)
        mcsc = graph_to_gates.clauses
        mcsg = graph_to_gates.minimal_corrected_gates()
        print(f'\tMinimal corrective subset clause: {mcsc}')
        print(f'\tMinimal corrective gate subset: {mcsg}')
        post.write_graph(f'{sd}RUN{i}_GRAPH_{problem}')
        inverse_transform = {
            'min_corr_sub_clauses': mcsc,
            'min_corr_sub_gates': mcsg
        }

        if os.path.exists(exact_wd):
            file = find_match(exact_wd, f'RUN{i}_RESULT_{problem}.json')
            if not file: break
            exact_result = json.load(open(f'{exact_wd}{file[0]}', 'r'))
            exact_bs = exact_result['eigenstate']
            ee = Error.expectation_sampler(cost, exact_bs)
            exact_gtg = Graph([Postprocess.coloured_nodes(exact_bs)], wcnf.soft, circuit, T.vertex_enc)
            print(f'\tExact Expectation for {exact_bs}({exact_gtg.clauses}, {exact_gtg.minimal_corrected_gates()}): {ee}')
            ae = Error.expectation_sampler(cost, approx_bs)
            print(f'\tApproximate Expectation for {approx_bs}: {ae}\n')

            mis_objective = Error.mis_obj(exact_bs)
            approx_mis_objective = Error.mis_obj(approx_bs)
            max_f = Error.max_factor(mis_objective, approx_mis_objective)
            ind_f = Error.ind_factor(G, most_sampled)
            mis_f = Error.mis_factor(ind_f, max_f)
            
            error_results = {
                    '<exact>': ee,
                    '<approx>': ae,
                    'exact_obj_cost': mis_objective,
                    'approx_obj_cost': approx_mis_objective,
                    'm_factor': max_f,
                    'is_factor': ind_f,
                    'mis_factor': mis_f
            }
            print(f'Error: \n{error_results}\n')
            write_result(f'{sd}RUN{i}_ERROR_{problem}', error_results)
        else:
            error_results = dict()
        
        optimiser_results = {
                'ansatz': ansatz.__class__.__name__,
                'optimiser': opt.__class__.__name__,
                'backend': backend.__class__.__name__,
                'qpu': qpu,
                '#qubits': n,
                '#parameters': len(result.optimal_point),
                '#cnots': ansatz.count_ops().get('cx'),
                'width': ansatz.width(),
                'depth': ansatz.depth(),
                'optimiser_evaluations': qt.counts,
                'energy': qt.means,
                'qpu_time': None,
                'parameters': [param for param in qt.params]
        }
        write_result(f'{sd}RUN{i}_OPTIMISER_{problem}', optimiser_results)

        problem_definition = {
                'circuit': circuit,
                'n_gates': len(circuit),
                'problem_name': f'Run {i} for {problem}'
        }
        post.write_result(f'{sd}RUN{i}_RESULT_{problem}', 
                problem_definition,
                inverse_transform,
                error_results,
                optimiser_results)
        
        plt.plot(qt.counts, qt.means)
        plt.ylabel('Energy')
        plt.xlabel('Optimiser Evaluations')
        plt.grid(color='gray', linestyle='--')
        plt.savefig(f'{sd}RUN{i}_OPTIMISER_{problem}', bbox_inches='tight')
        tikzplotlib.save(f'{td}RUN{i}_OPTIMISER_{problem}.tex')
        plt.close()
       
        plt.barh([*post.likely_states], np.abs([*post.likely_states.values()])**2)
        plt.ylabel('Sampled Bitstrings')
        plt.xlabel('Probability')
        plt.grid(color='gray', linestyle='--')
        plt.savefig(f'{sd}RUN{i}_HISTOGRAM_{problem}', bbox_inches='tight')
        tikzplotlib.save(f'{td}RUN{i}_HISTOGRAM_{problem}.tex')
        plt.close()
        
        post.draw_graphs()
        plt.savefig(f'{sd}RUN{i}_GRAPH_{problem}', bbox_inches='tight')
        plt.close()
        
        cct = vqe.ansatz
        cct.draw(output='mpl', justify='left', plot_barriers=False, 
                filename=f'{sd}RUN{i}_ANSATZ_{problem}', style='iqx')
        plt.close()
        
        cct.draw(output='latex_source', fold=-1, justify='left', plot_barriers=False, 
                filename=f'{td}RUN{i}_ANSATZ_{problem}.tex')

        style = dict()
        style['node_size'] = 0.5
        style['node_label'] = [_ for _ in G.nodes()]
        style['node_color'] = ['gray' if node in most_sampled 
                else 'white' for node in G.nodes()]
        #style['node_style'] = {'LineWidth=0.5pt'}
        #style['edge_style'] = {LineWidth=0.5pt}
        style['node_opacity'] = 0.5
        style['edge_curved'] = 0
        plot(G, f'{td}RUN{i}_GRAPH_{problem}.tex', layout=nx.drawing.layout.circular_layout(G), **style)
        
        if backend.__class__.__name__ == 'IBMQBackend':
            fig = plot_error_map(backend)
            fig.savefig(f'{sd}RUN{i}_MAP_{problem}', bbox_inches='tight')
            fig.clf()
            ansatz, t_in_sys = job_summary(j_ids)
            optimiser_results["#cnots"] = ansatz.count_ops().get('cx')
            optimiser_results["width"] = ansatz.width()
            optimiser_results["depth"] = ansatz.depth()
            optimiser_results["qpu_time"] = t_in_sys
            write_result(f'{sd}RUN{i}_OPTIMISER_{problem}', optimiser_results)
            break
        
        gset -= set(most_sampled)
        if not gset:
            print(f'Final solution: {sol_hist}')
            break
        bias = random.choice(tuple(gset))
        print(f'Biasing node: {bias}')
        if ansatz_type == 'Custom': 
            params = ParameterVector(r'\theta', length=n*10)
            it = iter(params)
            ansatz = QuantumCircuit(n)
            for q in qubits:
                if q in on: ansatz.x(q)
            ansatz.x(bias)
            for group in mit.consecutive_groups(new_qubits):
                qg = list(group)
                if len(qg) < 2:
                    ansatz.rx(next(it), qg[0])
                else:
                    for q in qg[:-1]: 
                        ansatz.cx(q, q + 1)
                    ansatz.cx(qg[-1], qg[0])
            for q in new_qubits: 
                ansatz.ry(next(it), q)
            ansatz = ansatz.reverse_bits()
        cost = Problems.vqe_max_independent_set(G, on=on, off=off, bias=bias)

    graph_to_gates = Graph(sol_hist, wcnf.soft, circuit, T.vertex_enc)
    print(f'\nMinimal corrective subset clauses: {graph_to_gates.clauses}')
    print(f'Minimal corrective gate subset: {graph_to_gates.minimal_corrected_gates()}\n')
