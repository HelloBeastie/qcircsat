from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy import stats
import tikzplotlib
import fnmatch
import json
import os

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
pd.set_option('display.expand_frame_repr', False)

def cubic(x, a, b, c, d):
    return a*x**3 + b*x**2 + c*x**1 + d*x**0

def str_cubic(a, b, c, d):
    return f'$\\num{{{a:.2e}}}x^3 + \\num{{{b:.2e}}}x^2 + \\num{{{c:.2e}}}x + \\num{{{d:.2e}}}$'

def quadratic(x, a, b, c):
    return a*x**2 + b*x**1 + c*x**0

def str_quadratic(a, b, c):
    return f'$\\num{{{a:.2e}}}x^2 + \\num{{{b:.2e}}}x + \\num{{{c:.2e}}}$'

def exp(x, a):
    return a**(x)

def str_exp(a):
    return f'${a:.2f}^x$'

def num_clauses(circuits):
    num_clauses = list()
    for c in circuits:
        cl_sum = 0
        for k,v in c.items():
            cl_sum += len(v)
        num_clauses.append(cl_sum)
    return num_clauses

def table_view(data_dict, cols=None, num_rows=10):
    for k,v in data_dict.items():
        print(k)
        if cols:
            try:
                print(v[cols].head(num_rows))
            except:
                pass
        else:
            print(v.head(num_rows))
        print('\n')

def cols_to_csv(data_dict, cols=None):
    for k,v in data_dict.items():
        if cols:
            v[cols].to_csv(f'csvs/{k}.csv', index=False)

def remove_outliers(data_dict, col):
    new_data_dict = dict()
    for k,v in data_dict.items():
        new_data_dict.update(
            {k: v[(np.abs(stats.zscore(v[col])) < 1)]}
        )
    return new_data_dict

def normalise(arr):
    arr = np.array(arr)
    m_arr = arr[arr != None]
    mn = np.min(m_arr)
    mx = np.max(m_arr)
    num = [i - mn if i is not None else None for i in arr] 
    return [float(j)/(mx-mn) if j is not None else None for j in num]

def none_to_nan(arr):
    return np.array(arr, dtype=float)

def shift_start(arr):
    arr = none_to_nan(arr) 
    start = arr[0]
    arr = arr - start + 1e-05 # 1e-05 is m22 lowest range
    return arr

def read_result(filename):
    with open(f'{filename}', 'r') as _:
        return json.load(_)

def find_match(directory, string):
    files = list()
    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, string):
            files.append(file)
    return files

def read_data(directory, string):
    files = sorted(find_match(directory, string))
    data = [read_result(f'{directory}{f}') for f in files]
    return data

def data_to_df():
    columns = [
            '<exact>',
            '<approx>',
            'depth',
            '#cnots',
            '#parameters',
            '#qubits',
            'min_corr_sub_cl',
            'min_corr_sub_clauses',
            'min_corr_sub_gates',
            'oracle_time',
            'num_gates',
            'optimiser_time',
            'num_vertices',
            'problem_name',
            'circuit',
            'n_gates',
            'm_factor',
            'is_factor',
            'mis_factor',
            'ansatz',
            'optimiser',
            'qpu_time',
            'qpu'
    ]
    problems = [
            'onetwodecoder',
            'halfadder',
            'halfsubtractor',
            'twoonemux',
            'fulladder',
            'twofourdecoder',
            'fullsubtractor'
    ]
    solvers = [
            'minisat22',
            'eigensolver',
            'qaoa',
            'cvqe',
            'vqe',
            'rtvqe',
    ]
    m, e, q, v, qv, rt = dict(), dict(), dict(), dict(), dict(), dict()
    for p in problems:
        wd = f'circuits/{p}'
        if not os.path.exists(f'circuits/{p}'):
            os.makedirs(f'{wd}tex/')
        for s in solvers:
            if os.path.exists(f'{wd}/{s}/'):
                data = read_data(f'{wd}/{s}/', '*RESULT*.json')
            df = pd.DataFrame.from_dict(data)
            if s == 'minisat22':
                m.update({
                    f'{p}.{s}': df
                })
            if s == 'eigensolver':
                e.update({
                    f'{p}.{s}': df
                })
            if s == 'qaoa':
                q.update({
                    f'{p}.{s}': df
                })
            if s == 'cvqe':
                v.update({
                    f'{p}.{s}': df
                })
            if s == 'vqe':
                qv.update({
                    f'{p}.{s}': df
                })
            if s == 'rtvqe':
                rt.update({
                    f'{p}.{s}': df
                })
    return m, e, q, v, qv, rt

def get_ordered_array(dict_of_df, field):
    ordered_array = list()
    for k,v in dict_of_df.items():
        if field in v.columns:
            ordered_array.append(v[field].values)
        else:
            ordered_array.append(None)
    return ordered_array

def get_aggregates(dict_of_df, field, agg):
    ordered_array = get_ordered_array(dict_of_df, field)
    if agg == 'mean':
        return np.array([np.mean(a) if a is not None else None for a in ordered_array])
    elif agg == 'max':
        return np.array([np.max(a) if a is not None else None for a in ordered_array])
    elif agg == 'min':
        return np.array([np.min(a) if a is not None else None for a in ordered_array])
    elif agg == 'first':
        return np.array([a[0] if a is not None else None for a in ordered_array])

def mpl(arr1, arr2, label, ptype, axlabels, color=None):
    arr1, arr2 = zip(*sorted(zip(arr1, arr2))) # sort both vectors keeping mapping
    if ptype == 'barh':
        plt.barh(arr1, arr2, label=label)
    if ptype == 'line':
        plt.plot(arr1, arr2, marker='o', alpha=0.7, label=label, linestyle='dashed', color=color)
    if ptype == 'scatter':
        plt.scatter(arr1, arr2, marker='o', alpha=0.7, label=label, color=color)
    if ptype == 'sline':
        plt.plot(arr1, arr2, alpha=0.7, label=label, color=color)
    if ptype == 'ylog':
        plt.yscale('log')
        plt.plot(arr1, arr2, marker='o', alpha=0.7, label=label, linestyle='dashed')
    if ptype == 'bar':
        plt.bar(arr1, arr2, label=label)
    plt.ylabel(axlabels[0])
    plt.xlabel(axlabels[1])
    #plt.legend(loc='upper left', bbox_to_anchor=(0.0, 1.15))
    plt.legend()
    plt.grid(color='gray', linestyle='--')

def mpl_bars(arr1, arr2s, labels, axlabels, axtype='log', width=1.4, bottom=0):
    if axtype == 'log':
        plt.yscale('log')
        bottom = 0.1
    bars = len(arr2s)
    for i,arr2 in enumerate(arr2s):
        w = (i - bars/2)*width + width/2
        plt.bar(arr1 + w, arr2, width=width*(1 - 0.1), align='center', 
                label=labels[i], bottom=bottom)
    plt.ylabel(axlabels[0])
    plt.xlabel(axlabels[1])
    plt.legend()
    #plt.legend(loc='upper left', bbox_to_anchor=(0.0, 1.15))
    plt.grid(color='gray', linestyle='--')

print('This script looks for data under circuit/solver/ directories ' \
        + 'so it suggested you put the files you want it to see there manually')
m, e, q, v, qv, rt = data_to_df()

sd = f'circuits/summary/'
if not os.path.exists(f'{sd}'):
    os.makedirs(f'{sd}')

table_view(m, cols=['min_corr_sub_cl', 'problem_name'])
table_view(e, cols=['min_corr_sub_cl', 'problem_name'])

cols_to_csv(m, cols=['problem_name', 'min_corr_sub_cl'])
cols_to_csv(e, cols=['problem_name', 'min_corr_sub_cl'])

#m_gates = get_aggregates(m, 'n_gates', 'max')
#rt_qubits = get_aggregates(rt, '#qubits', 'max')
#
#summary_name = 'gates_vs_qubits'
#axlabels = [r'Number of Qubits', r'Number of Gates in Combinational Logic Circuit']
#mpl(m_gates, rt_qubits, None, 'bar', axlabels)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#rt_optimiser_time = get_aggregates(rt, 'optimiser_time', 'mean')
#rt_qpu_time = get_aggregates(rt, 'qpu_time', 'mean')
#
#summary_name = 'qpu_vs_optimiser_time'
#axlabels = [r'Log Mean Time (s)', r'Number of Qubits']
#mpl(rt_qubits, rt_optimiser_time, 'Optimiser', 'ylog', axlabels)
#mpl(rt_qubits, rt_qpu_time, 'QPU', 'ylog', axlabels)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#e_optimiser_time = get_aggregates(e, 'optimiser_time', 'mean')
#q_optimiser_time = get_aggregates(q, 'optimiser_time', 'mean')
#v_optimiser_time = get_aggregates(v, 'optimiser_time', 'mean')
#
#summary_name = 'classical_graph_min_time_complexity'
#axlabels = [r'Mean Time Normalised', r'Number of Qubits']
#mpl(rt_qubits, normalise(e_optimiser_time), 'Exact Eigensolver', 'line', axlabels)
#mpl(rt_qubits, normalise(q_optimiser_time), 'QAOA Simulation', 'line', axlabels)
#mpl(rt_qubits, normalise(v_optimiser_time), 'VQE Simulation', 'line', axlabels)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#summary_name = 'solver_time_graph_complexity'
#axlabels = [r'Mean Time Normalised', r'Number of Qubits']
#mpl(rt_qubits, normalise(e_optimiser_time), 'Exact Eigensolver', 'scatter', axlabels, color='orange')
#mpl(rt_qubits, normalise(rt_optimiser_time), 'VQE Optimiser', 'scatter', axlabels, color='gray')
#
#plt.rcParams['text.usetex'] = True
#plt.rcParams['text.latex.preamble'] = r'\usepackage{siunitx}'
#
#params,_ = curve_fit(exp, rt_qubits[:-3], e_optimiser_time[:-3])
#fit = exp(rt_qubits[:-3], *params)
#mpl(rt_qubits[:-3], normalise(fit), str_exp(*params), 'sline', axlabels, color='orange')
#
#params,_ = curve_fit(quadratic, rt_qubits, rt_optimiser_time)
#fit = quadratic(rt_qubits, *params)
#mpl(rt_qubits, normalise(fit), str_quadratic(*params), 'sline', axlabels, color='gray')
#
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#m_oracle_time = get_aggregates(m, 'oracle_time', 'mean')
#
#summary_name = 'solver_time_complexity'
#axlabels = [r'Mean Time Normalised', r'Number of Combinational Logic Gates']
#mpl(m_gates, normalise(m_oracle_time), 'M22', 'scatter', axlabels, color='blue')
#mpl(m_gates, normalise(e_optimiser_time), 'Exact Eigensolver', 'scatter', axlabels, color='orange')
#mpl(m_gates, normalise(rt_optimiser_time), 'VQE Optimiser', 'scatter', axlabels, color='gray')
#
#params,_ = curve_fit(quadratic, m_gates, m_oracle_time)
#fit = quadratic(m_gates, *params)
#mpl(m_gates, normalise(fit), str_quadratic(*params), 'sline', axlabels, color='blue')
#
#params,_ = curve_fit(exp, m_gates[:-3], e_optimiser_time[:-3])
#fit = exp(m_gates[:-3], *params)
#mpl(m_gates[:-3], normalise(fit), str_exp(*params), 'sline', axlabels, color='orange')
#
#params,_ = curve_fit(quadratic, m_gates, rt_optimiser_time)
#fit = quadratic(m_gates, *params)
#mpl(m_gates, normalise(fit), str_quadratic(*params), 'sline', axlabels, color='gray')
#
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#circuits = get_aggregates(m, 'circuit', 'first')
#n_clauses = np.array(num_clauses(circuits))
#
#summary_name = 'clauses_time_complexity'
#axlabels = [r'Mean Time Normalised', r'Number of Clauses']
#mpl(n_clauses, normalise(m_oracle_time), 'M22', 'scatter', axlabels, color='blue')
#mpl(n_clauses, normalise(e_optimiser_time), 'Exact Eigensolver', 'scatter', axlabels, color='orange')
#mpl(n_clauses, normalise(rt_optimiser_time), 'VQE Optimiser', 'scatter', axlabels, color='gray')
#
#params,_ = curve_fit(quadratic, n_clauses, m_oracle_time)
#fit = quadratic(n_clauses, *params)
#mpl(n_clauses, normalise(fit), str_quadratic(*params), 'sline', axlabels, color='blue')
#
#params,_ = curve_fit(exp, n_clauses[:-3], e_optimiser_time[:-3])
#fit = exp(n_clauses[:-3], *params)
#mpl(n_clauses[:-3], normalise(fit), str_exp(*params), 'sline', axlabels, color='orange')
#
#params,_ = curve_fit(quadratic, n_clauses, rt_optimiser_time)
#fit = quadratic(n_clauses, *params)
#mpl(n_clauses, normalise(fit), str_quadratic(*params), 'sline', axlabels, color='gray')
#
#plt.rcParams['text.usetex'] = False
#
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#summary_name = 'graph_time_complexity_log_shifted'
#axlabels = [r'Log Mean Time Shifted Start (s)', r'Number of Qubits']
#mpl(rt_qubits, shift_start(m_oracle_time), 'M22', 'ylog', axlabels)
#mpl(rt_qubits, shift_start(e_optimiser_time), 'Exact Eigensolver', 'ylog', axlabels)
#mpl(rt_qubits, shift_start(rt_optimiser_time), 'VQE Optimiser', 'ylog', axlabels)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#rt_depth = get_aggregates(rt, 'depth', 'max')
#q_depth = get_aggregates(q, 'depth', 'max')
#print(q_depth)
#
#summary_name = 'vertex_to_depth'
#axlabels = [r'Log Maximum Circuit Depth', r'Number of Vertices in Graph']
#mpl_bars(none_to_nan(rt_qubits), [none_to_nan(rt_depth), none_to_nan(q_depth)], ['VQE Ansatz Depth', 'QAOA Ansatz Depth'], axlabels)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#rt_m = get_aggregates(rt, 'm_factor', 'mean')
#rt_is = get_aggregates(rt, 'is_factor', 'mean')
#rt_mis = get_aggregates(rt, 'mis_factor', 'mean')
#
#summary_name = 'MIS_error_factors'
#axlabels = [r'Mean MIS Error Factors', r'Number of Vertices in Graph']
#mpl_bars(rt_qubits[:-3], [rt_m[:-3], rt_is[:-3], rt_mis[:-3]], ['Maximalism', 'Independence', 'Maximal Independence'], axlabels, axtype='linear', width=0.5)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#rt_cnots = get_aggregates(rt, '#cnots', 'max')
#rt_params = get_aggregates(rt, '#parameters', 'max')
#q_cnots = get_aggregates(q, '#cnots', 'max')
#q_params = get_aggregates(q, '#parameters', 'max')
#
#summary_name = 'gate_resource_scaling'
#axlabels = [r'Log Maximum Gates', r'Number of Vertices in Graph']
## Check if vqe is transpiled gates or just normal gates
#mpl_bars(rt_qubits, [rt_cnots, rt_params, none_to_nan(q_cnots), none_to_nan(q_params)], ['CNOT VQE', 'Parametrised VQE', 'CNOT QAOA', 'Parametrised QAOA'], axlabels, width=0.5)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#rt_exact = get_aggregates(rt, '<exact>', 'mean') # needs to be an average
#rt_eigenval = get_aggregates(rt, 'eigenvalue', 'mean')
#rt_approx = get_aggregates(rt, '<approx>', 'mean')
#v_exact = get_aggregates(v, '<exact>', 'mean')
#v_eigenval = get_aggregates(v, 'eigenvalue', 'mean')
#v_approx = get_aggregates(v, '<approx>', 'mean')
#q_exact = get_aggregates(q, '<exact>', 'mean')
#q_eigenval = get_aggregates(q, 'eigenvalue', 'mean')
#q_approx = get_aggregates(q, '<approx>', 'mean')
#e_exact = get_aggregates(e, 'eigenvalue', 'mean')
#
#summary_name = 'classical_expectations'
#axlabels = [r'Energy Expectation', r'Number of Vertices in Graph']
#mpl(rt_qubits, none_to_nan(v_exact), 'VQE Exact', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(v_eigenval), 'VQE Optimiser Eigenvalue', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(v_approx), 'VQE Approximate', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(q_exact), 'QAOA Exact', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(q_eigenval), 'QAOA Optimiser Eigenvalue', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(q_approx), 'QAOA Approximate', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(e_exact), 'Numpy Eigensolver', 'line', axlabels)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
#
#
#summary_name = 'quantum_vs_classical_expectations'
#axlabels = [r'Energy Expectation', r'Number of Vertices in Graph']
#mpl(rt_qubits, none_to_nan(rt_exact), 'VQE Exact', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(rt_eigenval), 'VQE Optimiser Eigenvalue', 'line', axlabels)
#mpl(rt_qubits, none_to_nan(rt_approx), 'VQE Approximate', 'line', axlabels)
#tikzplotlib.clean_figure()
#tikzplotlib.save(f'{sd}{summary_name}.tex')
#plt.show()
#plt.close()
