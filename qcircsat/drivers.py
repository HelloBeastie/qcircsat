# Copyright 2018-2020 Xanadu Quantum Technologies Inc.
# Modification Copyright 2020-2021 Peter Demetriou
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
    # http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import networkx as nx
from qiskit.opflow import I, X, Z
from qcircsat.qtools import Qtools

class Drivers:
    '''
    Adapted from Pennylane to return Qiskit compatible Hamiltonians.
    Creates cost drivers, allows specification whether coloured or 
    uncoloured nodes in a graph are assigned higher or lower energies. 
    Also allows specification whether adjacent nodes that are coloured 
    or uncoloured should be assigned higher or lower energies. These 
    Hamiltonians are usually used with others in order to fully specify 
    an optimisation problem and are versatile enough to cover all the 
    major graph optimisation problems commonly solved in Quantum Computing.
    '''

    @staticmethod
    def bit_driver(graph, b):
        nodes = graph.nodes
        coeffs = (1 if b == 1 else -1 for _ in nodes)
        ops = (Qtools.pauli_at(Z, at, nodes) for at in graph)
        return Qtools.hamiltonian(coeffs, ops)
    
    @staticmethod
    def edge_driver(graph, reward):
        nodes = graph.nodes
        edges = graph.edges
        coeffs = list()
        ops = list()
        if len(reward) == 0 or len(reward) == 4:
            coeffs = [1 for _ in nodes]
            ops = [I for v in nodes]
        else:
            reward = list(set(reward) - {'01'})
            sign = -1
            if len(reward) == 2:
                reward = list({'00', '10', '11'} - set(reward))
                sign = 1
            reward = reward[0]
            if reward == '00':
                for e in edges:
                    coeffs.extend([0.25 * sign, 0.25 * sign, 0.25 * sign])
                    ops.extend([Qtools.pauli_at(Z, e[0], nodes)@Qtools.pauli_at(Z, e[1], nodes),
                        Qtools.pauli_at(Z, e[0], nodes), Qtools.pauli_at(Z, e[1], nodes)])
            if reward == '10':
                for e in edges:
                    coeffs.append(-0.5 * sign)
                    ops.append(Qtools.pauli_at(Z, e[0], nodes)@Qtools.pauli_at(Z, e[1], nodes))
            if reward == '11':
                for e in edges:
                    coeffs.extend([0.25 * sign, -0.25 * sign, -0.25 * sign])
                    ops.extend([Qtools.pauli_at(Z, e[0], nodes)@Qtools.pauli_at(Z, e[1], nodes),
                        Qtools.pauli_at(Z, e[0], nodes), Qtools.pauli_at(Z, e[1], nodes)])
        return Qtools.hamiltonian(coeffs, ops)
