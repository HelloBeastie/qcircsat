from qcircsat.drivers import Drivers
from qcircsat.mixers import Mixers
from qcircsat.constraints import Constraints

class Problems:
    '''
    Provides the Hamiltonians for MIS in use 
    in VQE optimisation and the Hamiltonians 
    required for MIS in the constrained and 
    unconstrained QAOA optimisation case.
    Additionally allows switching nodes on 
    and off and biasing other nodes.
    '''

    @staticmethod
    def vqe_max_independent_set(G, on=None, off=None, bias=None):
        cost = 3*Drivers.edge_driver(G, ['10', '01', '00']) \
                + Drivers.bit_driver(G, 1)
        if on is not None:
            for t in on:
                cost += 5*Constraints.reward(G, t)
        if off is not None:
            for f in off:
                cost += 5*Constraints.penalise(G, f)
        if bias is not None:
            cost += 3*Constraints.reward(G, bias)
        return cost.reduce()
    
    @staticmethod
    def qaoa_max_independent_set(G, constrained=True, on=None, off=None, bias=None):
        if constrained:
            phase = Drivers.bit_driver(G, 1)
            mixer = Mixers.bit_flip_mixer(G, 0).reduce()
        else:
            phase = 3*Drivers.edge_driver(G, ['10', '01', '00']) \
                    + Drivers.bit_driver(G, 1)
            mixer = Mixers.x_mixer(G).reduce()
        if on is not None:
            for t in on:
                phase += 5*Constraints.reward(G, t)
        if off is not None:
            for f in off:
                phase += 5*Constraints.penalise(G, f)
        if bias is not None:
            phase += 5*Constraints.reward(G, bias)
        return phase.reduce(), mixer

