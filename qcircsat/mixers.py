# Copyright 2018-2020 Xanadu Quantum Technologies Inc.
# Modification Copyright 2020-2021 Peter Demetriou
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
    # http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import networkx as nx
from itertools import product
from functools import reduce
from qiskit.opflow import I, X, Z
from qcircsat.qtools import Qtools

class Mixers:
    '''
    Adapted from Pennylane to return Qiskit compatible Hamiltonians.
    Creates mixers, allows specification whether coloured or 
    uncoloured nodes in a graph are assigned higher or lower energies. 
    These Hamiltonians are usually used with others in order to full specify 
    an optimisation problem and are versatile enough to cover most of the 
    major graph optimisation problems commonly solved in Quantum Computing.
    '''

    @staticmethod
    def x_mixer(graph):
        nodes = graph.nodes
        coeffs = (1 for _ in nodes)
        ops = (Qtools.pauli_at(X, at, nodes) for at in nodes)
        return Qtools.hamiltonian(coeffs, ops)
    
    @staticmethod
    def bit_flip_mixer(graph, b):
        sign = 1 if b == 0 else -1
        coeffs = []
        ops = []
        nodes = graph.nodes
        for i in nodes:
            neighbours = list(graph.neighbors(i))
            degree = len(neighbours)
            n_ops = [[Qtools.pauli_at(X, i, nodes)]] + [[Qtools.pauli_at(I, n, nodes),
                Qtools.pauli_at(Z, n, nodes)] for n in neighbours]
            n_coeffs = [[1, sign] for n in neighbours]
            final_ops = (Qtools.compose_iter(m) for m in product(*n_ops))
            final_coeffs = ((0.5**degree)*reduce(lambda i,j: i*j, m)
                            for m in product(*n_coeffs))
            coeffs.extend(final_coeffs)
            ops.extend(final_ops)
        return Qtools.hamiltonian(coeffs, ops)

