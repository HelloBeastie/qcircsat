import networkx as nx
from qiskit.opflow import Z
from qcircsat.qtools import Qtools

class Constraints:
    '''
    Penalises or rewards certain nodes in cost 
    Hamiltonians constructed from graphs.
    '''

    @staticmethod
    def reward(graph, at):
        return 0.5*Qtools.pauli_at(Z, at, graph.nodes)

    @classmethod
    def penalise(cls, graph, at):
        return -cls.reward(graph, at)

