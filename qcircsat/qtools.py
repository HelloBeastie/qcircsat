import datetime
from functools import reduce
from qiskit.opflow import I

class Qtools:
    '''
    Defines common mathematical structures used 
    for quantum circuits and for Hamiltonians.
    '''
    
    def __init__(self):
        self.counts = list()
        self.means = list()
        self.stds = list()
        self.params = list()
    
    def cb(self, eval_count, parameters, mean, std):
        print(f'{datetime.datetime.now()}: Circuit evalution {eval_count} '
                f'with parameters {parameters} has mean energy {mean} '
                f'with deviation {std}')
        self.counts.append(eval_count)
        self.means.append(mean)
        self.stds.append(std)
        self.params.append(parameters.tolist())

    @staticmethod
    def tensor_iter(iter_ops):
        return reduce(lambda A,B: A^B, iter_ops)
    
    @staticmethod
    def compose_iter(iter_ops):
        return reduce(lambda A,B: A@B, iter_ops)
    
    @classmethod
    def pauli_at(cls, op, at, nodes):
        return cls.tensor_iter((I if at != n else op for n in sorted(nodes)))
    
    @staticmethod
    def hamiltonian(coeffs, ops):
        return reduce(lambda i,j: i + j, (c*o 
            for c,o in zip(coeffs, ops)))

import networkx as nx
import numpy as np
import json
import matplotlib.pyplot as plt

class Postprocess:
    '''
    Handles the plotting and processing of data 
    returned by the various solutions to make 
    the results of the runs clearer. Additionally 
    writes common data and specified extended data 
    to file.
    '''

    def __init__(self, result, graph):
        if isinstance(result, dict):
            self.result = {
                    'optimal_point': result['optimal_point'].tolist(),
                    'eigenvalue': result['eigenvalue'].real,
                    'optimiser_evals': result['optimizer_evals'],
                    'cost_function_evals': result['cost_function_evals'],
                    'optimiser_time': result['optimizer_time'],
                    'eigenstate': result['eigenstate']
            }
        else: # note that this breaks the numpy solver since it doesn't have an optimal point 
            # require seperate classes that inherit from a base class is the way to go
            self.result = {
                    'optimal_point': result.optimal_point.tolist(),
                    'eigenvalue': result.eigenvalue.real,
                    'optimiser_evals': result.optimizer_evals,
                    'cost_function_evals': result.cost_function_evals,
                    'optimiser_time': result.optimizer_time,
                    'eigenstate': result.eigenstate
            }
        self.graph = graph

    def write_result(self, filename, *argv):
        for arg in argv:
            self.result.update(arg)
        with open(f'{filename}.json', 'w') as _:
            json.dump(self.result, _, indent=4)

    def write_graph(self, filename):
        nx.write_edgelist(self.graph, f'{filename}.txt')

    @property
    def run_time(self):
        return self.result['optimiser_time']

    @property
    def energy(self):
        return self.result['eigenvalue']

    @property
    def likely_states(self):
        d = self.result['eigenstate']
        d_sorted = {k: v for k, v in sorted(d.items(), key=lambda i: i[1])}
        return {k: d_sorted[k] for k in [*d_sorted][-9:]}

    @property
    def most_likely(self):
        d = self.result['eigenstate']
        if not isinstance(d, dict):
            return [*d.sample(1)][0]
        else:
            return max(d, key=d.get)

    @staticmethod
    def bitstrings(graph, coloured_node_hist):
        zero_state = '0'*len(graph)
        bitstring_hist = list()
        for sol in coloured_node_hist:
            bitstring = ''
            for i, s in enumerate(zero_state):
                if i in sol: bitstring += '1'
                else: bitstring += '0'
            bitstring_hist.append(bitstring)
        return bitstring_hist

    @staticmethod
    def coloured_nodes(bitstring):
        return [i for i,b in enumerate([int(_) for _ in bitstring]) if b]

    def colour_map(self, bitstring):
        return ['g' if node in self.coloured_nodes(bitstring)
                    else 'c' for node in self.graph.nodes]

    def __draw_graph(self, cmap, ax):
        nx.draw_networkx(self.graph, node_color=cmap, node_size=200, 
                alpha=.8, ax=ax, pos=nx.circular_layout(self.graph))

    def draw_graph(self):
        k = self.most_likely
        cmap = self.colour_map(k)
        nx.draw_networkx(self.graph, node_color=cmap, node_size=200, 
                alpha=.8, pos=nx.circular_layout(self.graph))

    def draw_graphs(self):
        size = int(np.ceil(np.sqrt(len(self.likely_states))))
        fig, axs = plt.subplots(size, size, figsize=(18, size*4), sharex=True, sharey=True)
        if isinstance(axs, np.ndarray):
            for ax, k in zip(axs.flatten(), self.likely_states.keys()):
                cmap = self.colour_map(k)
                ax.set_title(f'Probability: {np.abs(self.likely_states[k])**2}')
                self.__draw_graph(cmap, ax)
        else: self.draw_graph()

from qiskit.opflow.state_fns import StateFn, CircuitStateFn
from qiskit.opflow.expectations import AerPauliExpectation
from qiskit.opflow.converters import CircuitSampler
from qiskit.utils import QuantumInstance
from qiskit import QuantumCircuit, Aer

class Error():
    '''
    Calculates the various expectations given solutions 
    over Hamiltonians as well as factors indicating the 
    quality of the MIS solution in terms of the result.
    '''

    @staticmethod
    def expectation_sampler(hamiltonian, bitstring):
        '''
        The following routine for evaluating expectations of operators in qiskit is taken from: 
        https://quantumcomputing.stackexchange.com/questions/12080
        '''
        psi = QuantumCircuit(len(bitstring))
        for i,_ in enumerate(reversed(bitstring)):
            if int(_): psi.x(i)

        psi = CircuitStateFn(psi)

        backend = Aer.get_backend('aer_simulator')
        instance = QuantumInstance(backend, shots=1)

        measurable = StateFn(hamiltonian, is_measurement=True)@psi
        expectation = AerPauliExpectation().convert(measurable)
        sampler = CircuitSampler(backend).convert(expectation)
        return sampler.eval().real

    @staticmethod
    def mis_obj(bitstring):
        return sum([int(v) for v in bitstring])

    @staticmethod
    def max_factor(exact_obj, approx_obj):
        return 1 - abs(exact_obj 
                - approx_obj)/exact_obj

    @staticmethod
    def ind_factor(graph, coloured_nodes):
        adherence = 0
        num_nodes = len(graph.nodes)
        for v in graph.nodes:
            n = set(graph.neighbors(v))
            s = set(coloured_nodes)
            if v not in coloured_nodes:
                adherence += 1
            elif not n.intersection(s):
                adherence += 1
        return adherence/num_nodes

    @staticmethod
    def mis_factor(ind_f, max_f):
        return ind_f*max_f
