import datetime
import json
import numpy as np
from pysat.formula import WCNF
from circsat.preprocess import Circuit
from itertools import combinations

# circuit specification
tt = np.array([[-1, -2, -3, -7], 
                [-1, -2, 3, -7],
                [-1, 2, -3, 7],
                [-1, 2, 3, 7],
                [1, -2, -3, -7],
                [1, -2, 3, 7],
                [1, 2, -3, -7],
                [1, 2, 3, 7]])
cs = 3 # truth table io column split

# SAT/UNSAT implementation
pe = ['ND1', 'ND2', 'ND3', 'ND4']
circuits = list()
errors = list()
for l in range(len(pe) + 1):
    for c in combinations(pe, l):
        ND1 = Circuit.NAND(2, 2, 4)
        ND2 = Circuit.NAND(1, 2, 5)
        ND3 = Circuit.NAND(3, 4, 6)
        ND4 = Circuit.NAND(5, 6, 7)
        if c:
            errors.append(''.join(list(c)))
        elif not c:
            errors.append('SAT')
        if 'ND1' in c:
            ND1 = Circuit.AND(2, 2, 4)
        if 'ND2' in c:
            ND2 = Circuit.AND(1, 2, 5)
        if 'ND3' in c:
            ND3 = Circuit.AND(3, 4, 6)
        if 'ND4' in c:
            ND4 = Circuit.AND(5, 6, 7)
        # expected implementation
        circuits.append(
                {
                    'ND1': ND1,
                    'ND2': ND2,
                    'ND3': ND3,
                    'ND4': ND4,
                }
        )

for w,row in enumerate(tt):
    inp = list(row[:cs])
    out = list(row[cs:])
    for circuit, error in zip(circuits, errors):
        cct = Circuit(inp, out, **circuit)
        CNF1 = cct.cct_cnf
        with open(f'C{error}W{w}.json', 'w') as _:
            _.write(json.dumps(circuit, indent=4))

        wcnf = WCNF()
        wcnf.extend(cct.constraints)
        wcnf.extend(CNF1, weights=cct.weights)
        wcnf.topw = cct.top_weight
        wcnf.to_file(f'E{error}W{w}.wcnf', [f'c {cs}'])
