import datetime
import json
import numpy as np
from pysat.formula import WCNF
from circsat.preprocess import Circuit
from itertools import combinations

# circuit specification
tt = np.array([[-1, -2, -3, -8, -5], 
                [-1, -2, 3, -8, 5],
                [-1, 2, -3, -8, 5],
                [-1, 2, 3, 8, -5],
                [1, -2, -3, -8, 5],
                [1, -2, 3, 8, -5],
                [1, 2, -3, 8, -5],
                [1, 2, 3, 8, 5]])
cs = 3 # truth table io column split

# SAT/UNSAT implementation
pe = ['X1', 'X2', 'A1', 'A2', 'O1']
circuits = list()
errors = list()
for l in range(len(pe) + 1):
    for c in combinations(pe, l):
        X1 = Circuit.XOR(1, 2, 4)
        X2 = Circuit.XOR(3, 4, 5)
        A1 = Circuit.AND(3, 4, 6)
        A2 = Circuit.AND(1, 2, 7)
        O1 = Circuit.OR(6, 7, 8)
        if c:
            errors.append(''.join(list(c)))
        elif not c:
            errors.append('SAT')
        if 'X1' in c:
            X1 = Circuit.XNOR(1, 2, 4)
        if 'A1' in c:
            A1 = Circuit.NAND(3, 4, 6)
        if 'O1' in c:
            O1 = Circuit.NOR(6, 7, 8)
        if 'X2' in c:
            X2 = Circuit.XNOR(3, 4, 5)
        if 'A2' in c:
            A2 = Circuit.NAND(1, 2, 7)
        # expected implementation
        circuits.append(
                {
                    'X1': X1,
                    'X2': X2,
                    'A1': A1,
                    'A2': A2,
                    'O1': O1
                }
        )

for w,row in enumerate(tt):
    inp = list(row[:cs])
    out = list(row[cs:])
    for circuit, error in zip(circuits, errors):
        cct = Circuit(inp, out, **circuit)
        CNF1 = cct.cct_cnf
        with open(f'C{error}W{w}.json', 'w') as _:
            _.write(json.dumps(circuit, indent=4))

        wcnf = WCNF()
        wcnf.extend(cct.constraints)
        wcnf.extend(CNF1, weights=cct.weights)
        wcnf.topw = cct.top_weight
        wcnf.to_file(f'E{error}W{w}.wcnf', [f'c {cs}'])
