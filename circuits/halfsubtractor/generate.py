import datetime
import json
import numpy as np
from pysat.formula import WCNF
from circsat.preprocess import Circuit
from itertools import combinations

# circuit specification
tt = np.array([[-1, -2, -4, -5], 
                [-1, 2, 4, 5],
                [1, -2, 4, -5],
                [1, 2, -4, -5]])
cs = 2 # truth table io column split

# SAT/UNSAT implementation
pe = ['X1', 'N1', 'A1']
circuits = list()
errors = list()
for l in range(len(pe) + 1):
    for c in combinations(pe, l):
        X1 = Circuit.XOR(1, 2, 4)
        N1 = Circuit.NOT(1, 3)
        A1 = Circuit.AND(2, 3, 5)
        if c:
            errors.append(''.join(list(c)))
        elif not c:
            errors.append('SAT')
        if 'X1' in c:
            X1 = Circuit.XNOR(1, 2, 4)
        if 'N1' in c:
            N1 = Circuit.BUFFER(1, 3)
        if 'A1' in c:
            A1 = Circuit.NAND(2, 3, 5)
        # expected implementation
        circuits.append(
                {
                    'X1': X1,
                    'N1': N1,
                    'A1': A1
                }
        )

for w,row in enumerate(tt):
    inp = list(row[:cs])
    out = list(row[cs:])
    for circuit, error in zip(circuits, errors):
        cct = Circuit(inp, out, **circuit)
        CNF1 = cct.cct_cnf
        with open(f'C{error}W{w}.json', 'w') as _:
            _.write(json.dumps(circuit, indent=4))

        wcnf = WCNF()
        wcnf.extend(cct.constraints)
        wcnf.extend(CNF1, weights=cct.weights)
        wcnf.topw = cct.top_weight
        wcnf.to_file(f'E{error}W{w}.wcnf', [f'c {cs}'])
