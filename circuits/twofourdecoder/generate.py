import datetime
import json
import numpy as np
from pysat.formula import WCNF
from circsat.preprocess import Circuit
from itertools import combinations

# circuit specification
tt = np.array([[-1, -2, -5, -6, -7, 8], 
                [-1, 2, -5, -6, 7, -8],
                [1, -2, -5, 6, -7, -8],
                [1, 2, 5, -6, -7, -8]])
cs = 2 # truth table io column split

# SAT/UNSAT implementation
pe = ['N1', 'N2', 'A1', 'A2', 'A3', 'A4']
circuits = list()
errors = list()
for l in range(len(pe) + 1):
    for c in combinations(pe, l):
        N1 = Circuit.NOT(1, 3)
        N2 = Circuit.NOT(2, 4)
        A1 = Circuit.AND(3, 4, 5)
        A2 = Circuit.AND(1, 4, 6)
        A3 = Circuit.AND(2, 3, 7)
        A4 = Circuit.AND(2, 1, 8)
        if c:
            errors.append(''.join(list(c)))
        elif not c:
            errors.append('SAT')
        if 'N1' in c:
            N1 = Circuit.BUFFER(1, 3)
        if 'N2' in c:
            N2 = Circuit.BUFFER(2, 4)
        if 'A1' in c:
            A1 = Circuit.NAND(3, 4, 5)
        if 'A2' in c:
            A2 = Circuit.NAND(1, 4, 6)
        if 'A3' in c:
            A3 = Circuit.NAND(2, 3, 7)
        if 'A4' in c:
            A4 = Circuit.AND(2, 1, 8)
        # expected implementation
        circuits.append(
                {
                    'N1': N1,
                    'N2': N2,
                    'A1': A1,
                    'A2': A2,
                    'A3': A3,
                    'A4': A4
                }
        )

for w,row in enumerate(tt):
    inp = list(row[:cs])
    out = list(row[cs:])
    for circuit, error in zip(circuits, errors):
        cct = Circuit(inp, out, **circuit)
        CNF1 = cct.cct_cnf
        with open(f'C{error}W{w}.json', 'w') as _:
            _.write(json.dumps(circuit, indent=4))

        wcnf = WCNF()
        wcnf.extend(cct.constraints)
        wcnf.extend(CNF1, weights=cct.weights)
        wcnf.topw = cct.top_weight
        wcnf.to_file(f'E{error}W{w}.wcnf', [f'c {cs}'])
